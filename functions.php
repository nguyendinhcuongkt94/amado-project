<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version'    => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce            = require 'inc/woocommerce/class-storefront-woocommerce.php';
	$storefront->woocommerce_customizer = require 'inc/woocommerce/class-storefront-woocommerce-customizer.php';

	require 'inc/woocommerce/class-storefront-woocommerce-adjacent-products.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
	require 'inc/woocommerce/storefront-woocommerce-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';

	require 'inc/admin/class-storefront-plugin-install.php';
}

/**
 * NUX
 * Only load if wp version is 4.7.3 or above because of this issue;
 * https://core.trac.wordpress.org/ticket/39610?cversion=1&cnum_hist=2
 */
if ( version_compare( get_bloginfo( 'version' ), '4.7.3', '>=' ) && ( is_admin() || is_customize_preview() ) ) {
	require 'inc/nux/class-storefront-nux-admin.php';
	require 'inc/nux/class-storefront-nux-guided-tour.php';

	if ( defined( 'WC_VERSION' ) && version_compare( WC_VERSION, '3.0.0', '>=' ) ) {
		require 'inc/nux/class-storefront-nux-starter-content.php';
	}
}

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */
remove_action('storefront_footer', 'storefront_credit',20);
remove_action('storefront_footer', 'storefront_footer_widgets',10);
add_action('storefront_footer','footer_top',10);
function footer_top(){
	require_once('footer/footertop.php');
}
add_action('storefront_footer','footer_bottom',20);
function footer_bottom(){
	require_once('footer/footerbottom.php');
}
add_filter( 'get_product_search_form' , 'woo_custom_product_searchform',100 );

/**
 * woo_custom_product_searchform
 *
 * @access      public
 * @since       1.0
 * @return      void
*/
function woo_custom_product_searchform( $form ) {

	$form = '<form role="search" method="get" id="searchform" action="' . esc_url( home_url( '/'  ) ) . '">
		<div>
			<input type="hidden" name="post_type" value="product" />
			<input type="search" type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . __( 'Type your keyword...', 'woocommerce' ) . '" />
            <button type="submit"><img src="/amado/wp-content/uploads/img/core-img/search.png" alt=""></button>
		</div>
	</form>';

	return $form;

}

function tp_custom_checkout_fields( $fields ) {
	unset( $fields['address_2'] );
	$fields['first_name']['placeholder'] = 'First Name';
	$fields['last_name']['placeholder'] = 'Last Name';
	$fields['company']['placeholder'] = 'Company Name';
	$fields['city']['placeholder'] = 'Town';
	$fields['state']['placeholder'] = 'First Name';
	$fields['postcode']['placeholder'] = 'Zip Code';
	$fields['address_1']['placeholder'] = 'Address';

	$fields['first_name']['label'] = '';
	$fields['last_name']['label'] = '';
	$fields['company']['label'] = '';
	$fields['city']['label'] = '';
	$fields['state']['label'] = '';
	$fields['postcode']['label'] = '';
	$fields['address_1']['label'] = '';
	$fields['country']['label'] = '';
	return $fields;
   }
add_filter( 'woocommerce_default_address_fields', 'tp_custom_checkout_fields' );

remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);

remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10);
add_action('woocommerce_single_product_summary','woocommerce_template_single_price',4);

remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_price');
add_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_price',4);

remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart');
add_action('add_button_cart','woocommerce_template_loop_add_to_cart');
remove_action('storefront_header','storefront_product_search',40);
add_action('storefront_before_site','storefront_product_search');
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
remove_action( 'woocommerce_after_single_product_summary', 'storefront_single_product_pagination', 30 );
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  if(isset($_POST['selected'])){
		$cols = $_POST['selected'];
	}
	else {
		$cols = 4;
	}
  return $cols;
}

// add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {

    echo '<div id="my_custom_checkout_field"><h2>' . __('My Field') . '</h2>';

    woocommerce_form_field( 'my_field_name', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => __('Fill in this field'),
        'placeholder'   => __('Enter something'),
        ), $checkout->get_value( 'my_field_name' ));

    echo '</div>';

}
remove_action('woocommerce_before_shop_loop','woocommerce_result_count',20);
add_action('Show_total_product_perpage','woocommerce_result_count');
add_action('woocommerce_show_total_product','get_value_total_product',40);
function get_value_total_product(){
	$arr_total_product = [
			'12' => 12,
			'24' => 24,
			'48' => 48,
			'96' => 96,
			];
	wc_get_template('loop/selected-total-product-show.php',['data'=>$arr_total_product]);
}
remove_action('woocommerce_before_shop_loop_item_title','woocommerce_show_product_loop_sale_flash',10);
