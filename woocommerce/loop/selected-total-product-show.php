<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<div class="view-product d-flex align-items-center">
  <p>View</p>
	<form class="woocommerce-ordering" method="post" style="margin-bottom: 0px">
		<select name="selected" class="orderby" aria-label="<?php esc_attr_e( 'Shop order', 'woocommerce' ); ?>">
			<?php foreach ( $args['data'] as $key => $value ) : ?>
				<option
				<?php if($_POST['orderby']){if($_POST['orderby']==$value){echo 'selected';}}else { if($value == 12) { echo 'selected';} }?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
			<?php endforeach; ?>
		</select>
		<input type="hidden" name="paged" value="1" />
		<?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
	</form>
</div>
</div>
<!-- end-sorting -->
