<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */



?>
	<div class="cart-table-area section-padding-100">
			<div class="container-fluid">
				<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
					<div class="row">
							<div class="col-12 col-lg-8">
									<div class="checkout_details_area mt-50 clearfix">

											<div class="cart-title">

												<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

													<h2><?php esc_html_e( 'Checkout', 'woocommerce' ); ?></h2>

												<?php else : ?>

													<h2><?php esc_html_e( 'Checkout', 'woocommerce' ); ?></h2>

												<?php endif; ?>

											</div>

												<div class="row">
												<?php

												 if ( $checkout->get_checkout_fields() ) : ?>

													<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

															<?php do_action( 'woocommerce_checkout_billing' ); ?>

															<?php do_action( 'woocommerce_checkout_shipping' ); ?>

													<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

												<?php endif; ?>
												</div>
									</div>
							</div>
							<div class="col-12 col-lg-4">
										<div class="cart-summary">
												<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>

												<h5 id="order_review_heading"><?php esc_html_e( 'Cart total', 'woocommerce' ); ?></h5>

												<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

													<?php do_action( 'woocommerce_checkout_order_review' ); ?>

												<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

											<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
										</div>
							</div>
					</div>
				</form>
			</div>
	</div>
</div>
