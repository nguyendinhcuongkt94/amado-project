<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package storefront
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
$order = 'asc';
$hide_empty = false;
$cat_args = array(
    'order' => $order,
    'hide_empty' => $hide_empty,
);
$product_categories = get_terms('product_cat', $cat_args);
?>

<div class="shop_sidebar_area">
<!-- ##### Single Widget ##### -->
	<div class="widget catagory mb-50">
		<!-- Widget Title -->
		<h6 class="widget-title mb-30">Categories</h6>
		<!--  Catagories  -->

		<div class="catagories-menu">
				<ul>
					<?php if(!empty($product_categories)): ?>
					<?php foreach ($product_categories as $key => $category): ?>
						<li <?php if($category->name=='Chairs'){ echo "class='active'";}?>><a href="/amado/product-category/<?php echo $category->name; ?>"><?php echo $category->name; ?></a></li>
					<?php endforeach;
							endif;
					 ?>
				</ul>
		</div>
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
<!-- #secondary -->
	</div>
	<!-- ##### Single Widget ##### -->
	<div class="widget brands mb-50">
			<!-- Widget Title -->
			<h6 class="widget-title mb-30">Brands</h6>

			<div class="widget-desc">
					<!-- Single Form Check -->
					<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="amado">
							<label class="form-check-label" for="amado">Amado</label>
					</div>
					<!-- Single Form Check -->
					<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="ikea">
							<label class="form-check-label" for="ikea">Ikea</label>
					</div>
					<!-- Single Form Check -->
					<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="furniture">
							<label class="form-check-label" for="furniture">Furniture Inc</label>
					</div>
					<!-- Single Form Check -->
					<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="factory">
							<label class="form-check-label" for="factory">The factory</label>
					</div>
					<!-- Single Form Check -->
					<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="artdeco">
							<label class="form-check-label" for="artdeco">Artdeco</label>
					</div>
			</div>
	</div>

	<!-- ##### Single Widget ##### -->
	<div class="widget color mb-50">
			<!-- Widget Title -->
			<h6 class="widget-title mb-30">Color</h6>

			<div class="widget-desc">
					<ul class="d-flex">
							<li><a href="#" class="color1"></a></li>
							<li><a href="#" class="color2"></a></li>
							<li><a href="#" class="color3"></a></li>
							<li><a href="#" class="color4"></a></li>
							<li><a href="#" class="color5"></a></li>
							<li><a href="#" class="color6"></a></li>
							<li><a href="#" class="color7"></a></li>
							<li><a href="#" class="color8"></a></li>
					</ul>
			</div>
	</div>

</div>
