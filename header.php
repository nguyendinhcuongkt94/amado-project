<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Amado - Furniture Ecommerce Template | Home</title>

    <!-- Favicon  -->
    <link rel="icon" href="/amado/wp-content/uploads/img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="/amado/wp-content/css/core-style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <?php wp_head(); ?>
    <style media="screen">
      .form-row input, .form-row textarea, .form-row select {
        height: 55px;
        background-color: #f5f7fa;
        }
        #billing_note{
          height: 200px;
        }
        .cat-item{
          margin-bottom: 20px;
        }
        .cat-item a{
          color: #959595;
          font-size: 16px;
        }
        ul .cat-item a:hover {
          color: #fbb710;
        }
        .star-rating span:before, .quantity .plus, .quantity .minus, p.stars a:hover:after, p.stars a:after, .star-rating span:before, #payment .payment_methods li input[type=radio]:first-child:checked+label:before {
              color: #fbb710;
            }
        .container-fluid .row .col-12 nav {
          margin-bottom: 20px;
        }
        .container-fluid .row .col-12 nav a{
          color: black;
          font-size: 14px;
        }
        .amado-nav .menu ul{
          margin-left: 0px;
        }
        .main-content-wrapper .header-area .cart-fav-search a img {
          display: inline;
        }
        .col-12 .storefront-sorting {
          margin-bottom: 0px;
        }
        img {
          display: block;
          max-width: 100%;
          height: auto;
        }
        .price_slider_wrapper .price_slider span{
          background-color: #fff;
          top: -7px;
          width: 18px;
          height: 18px;
          margin: 0;
          border: 2px solid #fbb710;
          border-radius: 50%;
        }
        .onsale{
          background-color: red;
          border-color: red;
          color: white;          
        }
    </style>
</head>

<body <?php body_class(); ?>>
    <?php do_action( 'storefront_before_site' ); ?>

    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="/amado"><img src="/amado/wp-content/uploads/img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>
        <?php do_action( 'storefront_before_header' ); ?>
        <!-- Header Area Start -->
        <header class="header-area clearfix">
            <?php
		/**
		 * Functions hooked into storefront_header action
		 *
		 * @hooked storefront_header_container                 - 0
		 * @hooked storefront_skip_links                       - 5
		 * @hooked storefront_social_icons                     - 10
		 * @hooked storefront_site_branding                    - 20
		 * @hooked storefront_secondary_navigation             - 30
		 * @hooked storefront_product_search                   - 40
		 * @hooked storefront_header_container_close           - 41
		 * @hooked storefront_primary_navigation_wrapper       - 42
		 * @hooked storefront_primary_navigation               - 50
		 * @hooked storefront_header_cart                      - 60
		 * @hooked storefront_primary_navigation_wrapper_close - 68
		 */
		do_action( 'storefront_header' );
		?>

        </header>
        <!-- Header Area End -->

        <?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
