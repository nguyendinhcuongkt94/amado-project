<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package storefront
 */

get_header();
$order = 'asc';
$hide_empty = false;
$cat_args = array(
    'order' => $order,
    'hide_empty' => $hide_empty,
);
$product_categories = get_terms('product_cat', $cat_args);
function wh_get_min_price_per_product_cat($term_id)
{
    global $wpdb;

    $sql = "
    SELECT  MIN( meta_value+0 ) as minprice
    FROM {$wpdb->posts}
    INNER JOIN {$wpdb->term_relationships} ON ({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)
    INNER JOIN {$wpdb->postmeta} ON ({$wpdb->posts}.ID = {$wpdb->postmeta}.post_id)
    WHERE
      ( {$wpdb->term_relationships}.term_taxonomy_id IN (%d) )
        AND {$wpdb->posts}.post_type = 'product'
        AND {$wpdb->posts}.post_status = 'publish'
        AND {$wpdb->postmeta}.meta_key = '_price'
        AND {$wpdb->posts}.ID IN (SELECT posts.ID
                FROM {$wpdb->posts} AS posts
                INNER JOIN {$wpdb->term_relationships} AS term_relationships ON posts.ID = term_relationships.object_id
                INNER JOIN {$wpdb->term_taxonomy} AS term_taxonomy ON term_relationships.term_taxonomy_id = term_taxonomy.term_taxonomy_id
                INNER JOIN {$wpdb->terms} AS terms ON term_taxonomy.term_id = terms.term_id
                WHERE term_taxonomy.taxonomy = 'product_type'
                AND terms.slug = 'simple'
                AND posts.post_type = 'product')";
    return $wpdb->get_var($wpdb->prepare($sql, $term_id));
}
if (!empty($product_categories)) {
    echo '<div class="products-catagories-area clearfix">
	<div class="amado-pro-catagory clearfix">';
    foreach ($product_categories as $key => $category) {
		?>
				<li><?php echo $category->name ?></li>
				<!-- Single Catagory -->
				<div class="single-products-catagory clearfix">
					<a href="/amado/product-category/<?php echo $category->name; ?>">
						<img src="<?php echo wp_get_attachment_url(get_term_meta( $category->term_id, 'thumbnail_id', true )); ?>" alt="">
						<!-- Hover Content -->
						<div class="hover-content">
							<div class="line"></div>
							<p>From $<?php echo wh_get_min_price_per_product_cat($category->term_id); ?></p>
							<h4><?php echo $category->name ?></h4>
						</div>
					</a>
				</div>
		<?php
}
    echo '</div>
	</div>';
}

get_footer();
